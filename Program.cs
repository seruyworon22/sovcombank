﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TsendInvites
{
    class Program
    {
        static void Main(string[] args)
        {
           
        }
        /// <summary>
        /// Проверяет входные данные  и отправляет смс
        /// </summary>
        /// <param name="phone_numbers"></param>список мобильных номеров незарегистрированных пользователей.
        /// <param name="message"></param>текст сообщения со ссылкой.
        /// <returns></returns>
        static string sendInvites(string[] phone_numbers, string message)
            {
            try
            {
                //2.	Должен быть указан хотя бы один номер получателя приглашения. Иначе - возвращать ошибку с кодом 401 BAD_REQUEST PHONE_NUMBERS_EMPTY: Phone_numbers is missing.
                if (phone_numbers.Length == 0)
                    return "401 BAD_REQUEST PHONE_NUMBERS_EMPTY: Phone_numbers is missing.";

                //3.	Количество приглашаемых пользователей не может быть больше 16 в рамках одного вызова метода. Иначе - возвращать ошибку с кодом 402 BAD_REQUEST PHONE_NUMBERS_INVALID: Too much phone numbers, should be less or equal to 16 per request.
                if (phone_numbers.Length > 16)
                    return "402 BAD_REQUEST PHONE_NUMBERS_INVALID: Too much phone numbers, should be less or equal to 16 per request.";

                //4.	Суммарное количество приглашений в сутки не может превышать 128 запросов в рамках отдельно взятого приложения. Иначе - возвращать ошибку с кодом 403 BAD_REQUEST PHONE_NUMBERS_INVALID: Too much phone numbers, should be less or equal to 128 per day.
                if (getcountinvitations(4) >= 128 || getcountinvitations(4) + phone_numbers.Length > 128)
                    return "403 BAD_REQUEST PHONE_NUMBERS_INVALID: Too much phone numbers, should be less or equal to 128 per day.";

                //6.	Передаваемое сообщение не может быть пустым. 405 BAD_REQUEST MESSAGE_EMPTY: Invite message is missing.
                if (message == null || message == "")
                    return "405 BAD_REQUEST MESSAGE_EMPTY: Invite message is missing.";
                //7.	Допустимые символы в тексте сообщения должны соответствовать кодировке GSM 7-bit (03.38). Допускается наличие кириллических букв, которые преобразуются в латинские транслитерацией. 406 BAD_REQUEST MESSAGE_INVALID: Invite message should contain only characters in 7-bit GSM encoding or Cyrillic letters as well.
                // я вообще хз что делать с этой информацией, писать разбор кодировок ради тз я точно не буду
                string newstr = Translate(message);

                //8.Длинна сообщения не может быть больше 160 символов для латиницы и 128 символов для всех остальных. Иначе - возвращать ошибку с кодом 407 BAD_REQUEST MESSAGE_INVALID: Invite message too long, should be less or equal to 128 characters of 7 - bit GSM charset.
                if ((message != newstr && message.Length > 160) || (message == newstr && message.Length > 128))
                    return "407 BAD_REQUEST MESSAGE_INVALID: Invite message too long, should be less or equal to 128 characters of 7-bit GSM charset.";

                for (int i = 0; i < phone_numbers.Length; i++)
                {//11.	Если хотя бы один из номеров не проходит проверку, то СМС сообщение не отправляется никому, факт отправки приглашения в Системе при этом не фиксируется.
                    if (!condition1(phone_numbers[i]))
                    {//1.	Номера приглашаемых пользователей должны соответствовать международному формату с кодом страны 7 и длинной в 11 цифр (без пробелов, круглых скобок, и знака ‘+’). Иначе - возвращать ошибку с кодом 400 BAD_REQUEST PHONE_NUMBERS_INVALID: One or several phone numbers do not match with international format.
                        return "400 BAD_REQUEST PHONE_NUMBERS_INVALID: One or several phone numbers do not match with international format.";
                    }
                    if (!condition5(phone_numbers[i], phone_numbers, i))
                    {// 5.В списке номеров приглашаемых пользователей не должно быть дублей. 404 BAD_REQUEST PHONE_NUMBERS_INVALID: Duplicate numbers detected.
                        return "404 BAD_REQUEST PHONE_NUMBERS_INVALID: Duplicate numbers detected.";
                    }

                }


                // пройдены все проверки начинаем рассылать

                for (int i = 0; i < phone_numbers.Length; i++)
                {
                    Sendsms(phone_numbers[i], newstr);
                }

                //все отработало, фиксируем отправку в таблице
                // я бы фиксировал каждую строку внутри цикла отдельно, но раз в готовой функции на приеме массив то вот как то так
                // можно и в цикле приводить к массиву из одной строки, но смысла
                invite(7, phone_numbers);

                //ошибок небыло верну null 
                return null;
            }
            catch (Exception ex)
            {
                //9.	При отправке сообщений, в случае возникновения ошибок либо для исключительных ситуаций в коде возвращать ошибку с кодом 500 INTERNAL SMS_SERVICE: [описание ошибки] либо 500 INTERNAL [Имя приложения или объекта, вызвавшего ошибку]: [сообщение, описывающее исключение] соответственно.
                return "500 INTERNAL SMS_SERVICE: "+ ex.ToString();
            }


            
            }
        /// <summary>
        /// фиксирует факт отправки приглашений
        /// </summary>
        /// <param name="user_id"></param>id пользователя, автора записи (всегда равен 7);
        /// <param name="phones"></param>массив номеров приглашаемых пользователей.
        static void  invite( int user_id , string[] phones)
        {//не знаю зачем мне дали таблицу, если сами говорите что 13.	Факт отправки приглашения фиксируется записью в таблице
            // а функция фиксируюшая факт уже реализованна
        }
        /// <summary>
        ///  получает количество уже отправленных приглашений в течение дня и в рамках определённого приложения.
        /// </summary>
        /// <param name="apiid"></param>id приложения (всегда равно 4).
        /// <returns></returns>количество уже отправленных приглащений
        static int getcountinvitations( int apiid)
        {
            return 7;
        }
        /// <summary>
        /// отправка смс
        /// </summary>
        /// <param name="phone_numbers"></param>номер пользователя
        /// <param name="message"></param>сообщение
        static void Sendsms(string phone_numbers, string message)
        {
            //шлет смс 
        }
        /// <summary>
        /// 1.	Номера приглашаемых пользователей должны соответствовать международному формату с кодом страны 7 и длинной в 11 цифр (без пробелов, круглых скобок, и знака ‘+’). 
        /// </summary>
        /// <param name="phone_numbers"></param>//номер для проверки
        /// <returns></returns>//результат проверки
        static bool condition1(string phone_numbers)
        {
            if (phone_numbers.Length!=11 || phone_numbers[0]!='7' || phone_numbers.IndexOf(" ")!=-1 || phone_numbers.IndexOf("(") != -1 || phone_numbers.IndexOf(")") != -1 || phone_numbers.IndexOf("+") != -1)
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 5.	В списке номеров приглашаемых пользователей не должно быть дублей. 
        /// </summary>
        /// <param name="phone_numbers"></param> номер для проверки
        /// <param name="arrphone_numbers"></param> массив
        /// <returns></returns>результат проверки
        static bool condition5(string phone_numbers, string[] arrphone_numbers, int i)
        {

            for (int k = 0; k < i; k++)
            {
                if (phone_numbers== arrphone_numbers[k])
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// транслитерация
        /// </summary>
        /// <param name="str"></param>изначальная строка
        /// <returns></returns> после транслита
        static string Translate(string str)
        {
            string newstr = "";
            for (int i=0; i<str.Length ; i++)
            {
                switch (str[i])
                {
                    case 'а': newstr+="a"; break;
                    case 'б': newstr+= "b"; break;
                    case 'в': newstr+= "v"; break;
                    case 'г': newstr+= "g"; break;
                    case 'д': newstr+= "d"; break;
                    case 'е': newstr+= "e"; break;
                    case 'ё': newstr+= "ye"; break;
                    case 'ж': newstr+= "zh"; break;
                    case 'з': newstr+= "z"; break;
                    case 'и': newstr+= "i"; break;
                    case 'й': newstr+= "y"; break;
                    case 'к': newstr+= "k"; break;
                    case 'л': newstr+= "l"; break;
                    case 'м': newstr+= "m"; break;
                    case 'н': newstr+= "n"; break;
                    case 'о': newstr+= "o"; break;
                    case 'п': newstr+= "p"; break;
                    case 'р': newstr+= "r"; break;
                    case 'с': newstr+= "s"; break;
                    case 'т': newstr+= "t"; break;
                    case 'у': newstr+= "u"; break;
                    case 'ф': newstr+= "f"; break;
                    case 'х': newstr+= "ch"; break;
                    case 'ц': newstr+= "z"; break;
                    case 'ч': newstr+= "ch"; break;
                    case 'ш': newstr+= "sh"; break;
                    case 'щ': newstr+= "ch"; break;
                    case 'ъ': newstr+= "''"; break;
                    case 'ы': newstr+= "y"; break;
                    case 'ь': newstr+= "''"; break;
                    case 'э': newstr+= "e"; break;
                    case 'ю': newstr+= "yu"; break;
                    case 'я': newstr+= "ya"; break;
                    case 'А': newstr+= "A"; break;
                    case 'Б': newstr+= "B"; break;
                    case 'В': newstr+= "V"; break;
                    case 'Г': newstr+= "G"; break;
                    case 'Д': newstr+= "D"; break;
                    case 'Е': newstr+= "E"; break;
                    case 'Ё': newstr+= "Ye"; break;
                    case 'Ж': newstr+= "Zh"; break;
                    case 'З': newstr+= "Z"; break;
                    case 'И': newstr+= "I"; break;
                    case 'Й': newstr+= "Y"; break;
                    case 'К': newstr+= "K"; break;
                    case 'Л': newstr+= "L"; break;
                    case 'М': newstr+= "M"; break;
                    case 'Н': newstr+= "N"; break;
                    case 'О': newstr+= "O"; break;
                    case 'П': newstr+= "P"; break;
                    case 'Р': newstr+= "R"; break;
                    case 'С': newstr+= "S"; break;
                    case 'Т': newstr+= "T"; break;
                    case 'У': newstr+= "U"; break;
                    case 'Ф': newstr+= "F"; break;
                    case 'Х': newstr+= "Ch"; break;
                    case 'Ц': newstr+= "Z"; break;
                    case 'Ч': newstr+= "Ch"; break;
                    case 'Ш': newstr+= "Sh"; break;
                    case 'Щ': newstr+= "Ch"; break;
                    case 'Ъ': newstr+= "''"; break;
                    case 'Ы': newstr+= "Y"; break;
                    case 'Ь': newstr+= "''"; break;
                    case 'Э': newstr+= "E"; break;
                    case 'Ю': newstr+= "Yu"; break;
                    case 'Я': newstr+= "Ya"; break;
                    default:  newstr += str[i]; break;
                }
            }
            return newstr;
        }


    }
}
